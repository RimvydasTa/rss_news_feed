
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


$(function () {

    $(".modal-title").on("click", function () {
        var title = $(this).text();
        var text = $(this).next(".card-text").text();
        var linkHref = $(this).data( "link" );


        $("#exampleModalLabel").text(title);
        $(".modal-body").text(text);
        $(".modal-feed-link").attr('href', linkHref);
    });
});

