
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h2>{{ __('Feed categories') }}</h2></div>

                    <div class="card-body">

                        @if(count($feed_categories) === 0)
                            You have no categories.
                                <p>
                                    Create new category <a href="/feed_category/create"> here</a>
                                </p>
                            @else
                                @foreach($feed_categories as $feed_category)
                                    <div class="row mb-2">
                                        <div class="col-md-6">
                                            {{$feed_category->category_name}}
                                        </div>
                                        <div class="col-md-2">
                                            <a class="btn btn-info mr-1" href="/feed_category/{{$feed_category->id}}/edit">Edit</a>
                                        </div>
                                        <div class="col-md-2">
                                            <form action="/feed_category/{{$feed_category->id}}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-danger">Delete Category</button>
                                            </form>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>

                </div>
            </div>
        </div>
    </div>
@endsection
