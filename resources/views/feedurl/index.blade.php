
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h2>
                            {{ __('Feed urls') }}
                            <a href="/feed_url/create" class=" btn btn-info float-right"> Add feed url</a>
                        </h2>
                        </div>

                    <div class="card-body">
                        @if(count($feed_urls) === 0)
                            You have no feeds. Create new feed <a href="/feed_url/create">here</a>
                            @else
                            <ul>
                                @foreach($feed_urls as $feed_url)
                                    <li>
                                        <a href="/feed_url/{{$feed_url->id}}">
                                            {{$feed_url->url}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
