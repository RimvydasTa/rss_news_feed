@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h2>{{ __('Create a feed url') }}</h2></div>

                <div class="card-body">
                    <form method="POST" action="/feed_url">
                        @csrf

                        <div class="form-group row">
                            <label for="url_name" class="col-md-4 col-form-label text-md-right">{{ __('Url Name') }}</label>

                            <div class="col-md-6">
                                <input id="url_name" type="text" class="form-control{{ $errors->has('url_name') ? ' is-invalid' : '' }}" name="url_name" value="{{ old('url_name') }}" required autofocus>

                                @if ($errors->has('url_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('url_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="url" class="col-md-4 col-form-label text-md-right">{{ __('Url') }}</label>

                            <div class="col-md-6">
                                <input id="url" type="text" class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}" name="url" required>

                                @if ($errors->has('url'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create Feed Url') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection