@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><h2>{{ __('Feed url name: ') }} {{$feed_url->url_name}}</h2></div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 float-left">
                                Link: <a class="card-link" target="_blank" href="{{$feed_url->url}}">{{$feed_url->url}}</a>
                            </div>
                            <div class="col-md-6">
                                <div class="row float-right">
                                <a class="btn btn-info mr-1" href="/feed_url/{{$feed_url->id}}/edit">Edit</a>
                                <form action="/feed_url/{{$feed_url->id}}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger mr-3">Delete URL</button>
                                </form>
                                </div>
                            </div>
                        </div>
                        <div class="card-title">
                            <form action="/feed_url/{{$feed_url->id}}/category" method="POST">
                                @method('PATCH')
                                @csrf
                                <div class="form-group">
                                    <label for="feed_url_category">Category:</label>
                                    <select name="feed_url_category" class="form-control" id="feed_url_category" onchange="form.submit()">
                                        <option value="">
                                            @if($feed_url->feed_category_id !== 0)
                                                {{$categorie_name}}
                                                @else
                                                Select a category
                                                @endif
                                        </option>
                                        @foreach($categories as $category)
                                            @if($feed_url->feed_category_id !== $category->id)
                                                 <option value="{{$category->id}}">{{$category->category_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection