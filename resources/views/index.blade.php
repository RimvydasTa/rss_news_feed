@extends('layouts.home')

@section('content')
    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4">Welcome to rss news feed!
            </h1>
            @if(count($feeds) > 0)
                @foreach($feeds as $key => $feed)
                    <h1 id="{{$key}}" class="mb-3 feed-heading">
                        Feed provider: <a target="_blank" href="{{$feed[0]['link']}}">{{$key}}</a>
                    </h1>
                    @foreach($feed as $post)
                    <!-- Blog Post -->
                        <div class="card mb-4">
                            <div class="card-body">
                                <h2 class="card-title modal-title" data-toggle="modal" data-link="{{$post['link']}}"
                                    data-target="#feedModal">{{$post['title']}}</h2>
                                <p class="card-text">
                                    {!! \Illuminate\Support\Str::words($post['desc'], 50,'')  !!}
                                </p>

                                <div class="feed-link my-1">
                                    Feed provider:
                                    <a class="" target="_blank" href="{{$post['link']}}">{{$post['site']}}</a>
                                </div>
                            </div>
                            <div class="card-footer text-muted">
                                Posted on {{$post['date']}}

                            </div>
                        </div>
                    @endforeach
                @endforeach
            @else
                <div class="card">
                    <div class="card-body">
                        <p class="card-text">No feeds published</p>
                    </div>
                </div>
            @endif
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">
            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Categories</h5>
                <div class="card-body pb-5">
                    <div class="row">
                        @if(count($categories) > 0)
                            @foreach($categories as $category)
                                @if(count($category->feedUrls) > 0)
                                    <div class="col-lg-6">
                                        <ul class="list-unstyled mb-0">
                                            <li>
                                                <a href="/?cat_id={{$category->id}}">{{$category->category_name}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            @endforeach
                        @else
                            <div class="col-lg-6">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        No category has assosiated feeds.
                                        Please assign category to a feed
                                    </li>
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- Feeds Widget -->
            <div class="card my-4">
                <h5 class="card-header">Feed navigation</h5>
                <div class="card-body pb-5">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                @if(count($feeds) > 0)
                                    @foreach($feeds as $key => $feed)
                                        <li>
                                            <a href="#{{$key}}">{{$key}}</a>
                                        </li>
                                    @endforeach
                                @else
                                    <li>No feeds are published</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
@endsection