<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedUrlsTable extends Migration
{
    //https://www.dude.fi/feed
    //https://www.huurteinen.fi/feed/
    //https://rollemaa.org/leffat-rss.php
    //https://geekylifestyle.com/feed

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('feed_category_id')->default(0);
            $table->string('url_name')->unique();
            $table->string('url')->unique();
            $table->integer('published')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_urls');
    }
}
