<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedCategory extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'category_name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedUrls()
    {
        return $this->hasMany(FeedUrl::class);
    }
}
