<?php

namespace App\Http\Controllers;

use App\FeedUrl;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class FeedUrlController extends Controller
{

    private $categoryService;

    /**
     * FeedUrlController constructor.
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $feed_urls = FeedUrl::all();

        return view('feedurl.index', compact('feed_urls'));

    }

    /**
     * @param FeedUrl $feed_url
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(FeedUrl $feed_url)
    {
        $categories = $this->categoryService->getAllCategories();
        if ($feed_url->feed_category_id !== 0){
            $categorie_name = $this->categoryService->getCategoryNameByid($feed_url->feed_category_id);
            return view('feedurl.show', compact('feed_url', 'categories', 'categorie_name'));
        }else {
            return view('feedurl.show', compact('feed_url', 'categories'));
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {

        $attributes = request()->validate(
            [
                'url_name' => 'required|min:4|unique:feed_urls',
                'url' => 'required|min:8|unique:feed_urls']
        );

        FeedUrl::create($attributes);

        return redirect('/feed_url');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('feedurl.create');
    }

    /**
     * @param FeedUrl $feed_url
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(FeedUrl $feed_url)
    {
        return view('feedurl.edit', compact('feed_url'));

    }

    /**
     * @param FeedUrl $feed_url
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(FeedUrl $feed_url)
    {
        $feed_url->url_name = request('url_name');
        $feed_url->url = request('url');

        $feed_url->save();

        return redirect('/feed_url');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function  updateCategory($id)
    {
        $feedUrl = FeedUrl::find($id);

        if($feedUrl) {
            $feedUrl->feed_category_id = request('feed_url_category');
            $feedUrl->save();
        }

        return back();
    }

    /**
     * @param FeedUrl $feed_url
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(FeedUrl $feed_url)
    {
        $feed_url->delete();

        return redirect('/feed_url');
    }
}
