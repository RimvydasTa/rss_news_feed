<?php

namespace App\Http\Controllers;

use App\FeedCategory;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class FeedCategoryController extends Controller
{
    protected $categoryService;
    /**
     * FeedCategoryController constructor.
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {

        $feed_categories = FeedCategory::all();

        return view('category.index', compact('feed_categories'));

    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store()
    {

            $attributes = request()->validate(
                [
                    'category_name' => 'required|min:4|unique:feed_categories'
                ]);
            FeedCategory::create($attributes);

            return redirect('/feed_category');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * @param FeedCategory $feed_category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(FeedCategory $feed_category)
    {
        return view('category.edit', compact('feed_category'));

    }

    /**
     * @param FeedCategory $feed_category
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(FeedCategory $feed_category)
    {
        $feed_category->category_name = request('category_name');

        $feed_category->save();

        return redirect('/feed_category');
    }

    /**
     * @param FeedCategory $feed_category
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(FeedCategory $feed_category)
    {
        $this->categoryService->handleCategorieDelete($feed_category->id);
        $feed_category->delete();

        return redirect('/feed_category');
    }
}
