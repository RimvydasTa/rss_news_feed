<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use App\Services\FeedFetchService;
use Illuminate\Http\Request;

class FeedController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeds = app()->make(FeedFetchService::class)->getFeeds();
        $categories = app()->make(CategoryService::class)->getAllCategories();

        return view('index', compact('feeds'), compact('categories'));
    }


}
