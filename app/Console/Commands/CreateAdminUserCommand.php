<?php

namespace App\Console\Commands;


use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateAdminUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates admin user for rss feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $username = $this->ask('Enter desired username');
        $password = $this->secret('Enter desired password');

        $rez =  User::create([
            'username' => $username,
            'password' => Hash::make($password),
        ]);

        if ($rez){
            $this->info('user created');
        }else {
            $this->alert('something went wrong check db');
        }


    }
}
