<?php

namespace App\Console\Commands;

use App\FeedUrl;
use Illuminate\Console\Command;

class FeedUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates feeds so they would show up on the page';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $feedUpdate = FeedUrl::query()->update(['published' => 1]);

        if ($feedUpdate){
            $this->info('Feed updated');
        }else {
            $this->alert('Error something went wrong');
        }

    }
}
