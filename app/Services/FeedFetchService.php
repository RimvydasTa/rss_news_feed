<?php
/**
 * Created by PhpStorm.
 * User: Rimvydas
 * Date: 1/13/2019
 * Time: 19:20
 */

namespace App\Services;


use App\FeedCategory;
use App\FeedUrl;
use DOMDocument;
use phpDocumentor\Reflection\Types\Object_;

class FeedFetchService
{
    protected $DOMDocument;
    protected $urlArray;

    /**
     * @return array
     */
    public function getFeeds() : array
    {
        $this->DOMDocument = new DOMDocument();

        if (isset(request()->cat_id)) {
            $category_id = request()->cat_id;
            $categoryFeed = $this->getCategoryFeed($category_id);
            $this->urlArray = $this->buildUrlArray($categoryFeed);
        } else {
            $publishedFeeds = $this->getPublishedFeeds();
            $this->urlArray = $this->buildUrlArray($publishedFeeds);
        }

        $feeds = $this->assignFeedsToArray($this->urlArray);

        return $feeds;
    }

    /**
     * @return mixed
     */
    public function getPublishedFeeds()
    {
        $publishedFeeds = FeedUrl::where('published', 1)->get();

        return $publishedFeeds;
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    public function getCategoryFeed(string $categoryId) :object
    {
        $category = FeedCategory::find($categoryId);
        $categoryUrls = $category->feedUrls;

        return $categoryUrls;
    }

    /**
     * @param $feeds
     * @return array
     */
    public function buildUrlArray(object $feeds) : array
    {
        $rez = [];
        foreach ($feeds as $feed_item) {
            if ($feed_item->published === 1) {
                $rez[] = [
                    'name' => $feed_item->url_name,
                    'url' => $feed_item->url
                ];
            }
        }
        return $rez;
    }

    /**
     * @param $feedArray
     * @return array
     */
    public function assignFeedsToArray (array $feedArray) :array
    {
        $feed = [];
        foreach ($feedArray as $url) {

            $this->DOMDocument->load($url['url']);

            foreach ($this->DOMDocument->getElementsByTagName('item') as $key => $node) {

                $item = array(
                    'site' => $url['name'],
                    'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
                    'desc' => strip_tags($node->getElementsByTagName('description')->item(0)->nodeValue),
                    'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
                    'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
                );

                $feed[$item['site']][] = $item;
            }
        }
        return $feed;
    }

}
