<?php
/**
 * Created by PhpStorm.
 * User: Rimvydas
 * Date: 1/13/2019
 * Time: 17:10
 */

namespace App\Services;


use App\FeedCategory;
use App\FeedUrl;

class CategoryService
{

    /**
     * @return FeedCategory[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllCategories()
    {
        return FeedCategory::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCategoryNameByid($id)
    {

        $category = FeedCategory::find($id);

        return $category->category_name;
    }

    public function handleCategorieDelete($categorieId)
    {
        FeedUrl::where('feed_category_id', $categorieId)->update(array('feed_category_id' => 0));
    }
}
