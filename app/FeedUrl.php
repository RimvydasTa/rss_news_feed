<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedUrl extends Model
{
    //

    protected $fillable = [
        'url_name', 'url',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feedCategory()
    {
        return $this->belongsTo(FeedCategory::class);
    }
}
