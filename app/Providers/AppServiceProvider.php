<?php

namespace App\Providers;

use App\Services\CategoryService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Services\FeedFetchService;
use DOMDocument;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(FeedFetchService::class, function (){
        return new FeedFetchService();
    });

        $this->app->bind(CategoryService::class, function (){
            return new CategoryService();
        });

    }
}
