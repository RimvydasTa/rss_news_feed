<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'FeedController@index');
Route::group(['middleware' => ['auth']], function() {
    Route::resource('feed_url', 'FeedUrlController');
    Route::patch('feed_url/{id}/category','FeedUrlController@updateCategory' );
    Route::resource('feed_category', 'FeedCategoryController');
});

Auth::routes();
Route::get('/dashboard', 'AdminController@index')->name('dashboard');

