###Kaip pasileisti projekta?

1. Nusiklonuoti projektą iš 
    `https://bitbucket.org/RimvydasTa/rss_news_feed/src/master/`
2. Paleisti `composer install`
3. Paleisti `npm install`
4. Susikurti .env failą (galima nusikopijuoti .env.example) ir jame supildyti duoombazės prisijungimo duomenis
5. Paleisti `php artisan key:generate`
6. Paleisti `npm run dev`
7. Paleisti `php artisan migrate`

8. Lesti komanda `php artisan user:create` ir sukurti admin vartotją
9. Leisti `php artisan serve` ir naršyklėje tikrinti  `http://127.0.0.1:8000`

---
Atsidarius projektą reikia sukurti nuorodas iš kurių bus paimami feed postai.
Pradineme pusalpyje įrankių juostoje po dešine bus mygtukas login kuris nuves į
admin prisijungimo panelę.

Suveskite savo prisijungimus kuriuos sukūrėte naudojant `php artisan user:create`.

Prisijungus galite sukurti naują feed url spausdami 'Create feed url'
arba naują kategoriją spausdami 'Create new categorie'

Pateikiu pora pavyzdinių feed nuorodų: 

* `https://geekylifestyle.com/feed`
* `https://www.dude.fi/feed`   
* `https://www.huurteinen.fi/feed/`
* `https://rollemaa.org/leffat-rss.php`
---
Sukūrus nuorodą/as galima jas prisikirti kategorijai nueinant į įrankių juostos punktą
"Feeds" spaust ant norimos nuorodos ir naujame lange kategorijos skiltyje pasirinkti norimą
kategoriją

Norint sukurtų nuorodų įrašus atvaizduoti pradiniame puslapyje consolėje reikia leisti komandą `php artisan feed:update`

