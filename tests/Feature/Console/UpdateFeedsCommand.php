<?php

namespace Tests\Feature\Console;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateFeedsCommand extends TestCase
{

    /**
     * A basic test example.
     * @test
     * @return void
     */
    public function testFeedUpdate()
    {
        $this->artisan('feed:update')
        ->expectsOutput('Feed updated');

        $this->assertDatabaseMissing('feed_urls', ['published' => 0]);
    }
}
