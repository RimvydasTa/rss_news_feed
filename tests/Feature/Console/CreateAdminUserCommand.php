<?php

namespace Tests\Feature\Console;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateAdminUserCommand extends TestCase
{
    // vendor/bin/phpunit tests/Feature/Console/CreateAdminUserCommand.php
    /**
     * @test
     */
    public function testCreateAdminUser()
    {
        $this->artisan('user:create')

        ->expectsQuestion('Enter desired username', 'testAdmin')
        ->expectsQuestion('Enter desired password', 'secret')
        ->expectsOutput('user created');

        $this->assertDatabaseHas('users', ['username' => 'testAdmin']);

    }
}
